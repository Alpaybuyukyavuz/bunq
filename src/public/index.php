<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../../vendor/autoload.php';

$app = new \Slim\App;

$app->get('/', function () {
    phpinfo();
});

$app->get('/users', function (Request $request, Response $response, $args) {
    $pdo = new PDO('sqlite:chat.db');

    $stmt = $pdo->query('SELECT * FROM users');
    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $response->withJson($users);
});

$app->post('/users', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    $name = $data['username'];

    $pdo = new PDO('sqlite:chat.db');

    $stmt = $pdo->prepare('INSERT INTO users (username) VALUES (?)');
    $stmt->execute([$name]);

    return $response->withJson(['message' => 'User created successfully']);
});

$app->get('/messages/{receiver_id}', function (Request $request, Response $response, $args) {
    $receiver_id = $args['receiver_id'];

    $pdo = new PDO('sqlite:chat.db');

    $stmt = $pdo->prepare('SELECT messages.message, users.username AS sender_name, messages.timestamp
                           FROM messages JOIN users ON messages.sender_id = users.id
                           WHERE messages.receiver_id = ?
                           ORDER BY messages.timestamp DESC');
    $stmt->execute([$receiver_id]);
    $messages = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $response->withJson($messages);
});

$app->post('/messages', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    $sender_id = $data['sender_id'];
    $receiver_id = $data['receiver_id'];
    $message = $data['message'];

    $pdo = new PDO('sqlite:chat.db');

    $stmt = $pdo->prepare("INSERT INTO messages (message, sender_id, receiver_id) VALUES (?, ?, ?)");
    $stmt->bindParam(1, $message);
    $stmt->bindParam(2, $sender_id);
    $stmt->bindParam(3, $receiver_id);
    $stmt->execute();

});


$app->run();